$(document).ready(function(){
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-css.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    }

    const gNAME_COL = ['id', 'courseCode', 'courseName', 'duration', 'level', 'discountPrice', 'teacherName', 'action']
    const gID_COL = 0
    const gCOURSE_CODE_COL = 1
    const gCOURSE_NAME_COL = 2
    const gDURATION_COL = 3
    const gLEVEL_COL = 4
    const gDISCOUNT_PRICE_COL = 5
    const gTEACHER_COL = 6
    const gACTION_COL = 7
    
    var gDataTable = gCoursesDB.courses
    var gTableAllCourse = $('#tbl-allcourses').DataTable({
                                columns : [
                                    { data : gNAME_COL[gID_COL]},
                                    { data : gNAME_COL[gCOURSE_CODE_COL]},
                                    { data : gNAME_COL[gCOURSE_NAME_COL]},
                                    { data : gNAME_COL[gDURATION_COL]},
                                    { data : gNAME_COL[gLEVEL_COL]},
                                    { data : gNAME_COL[gDISCOUNT_PRICE_COL]},
                                    { data : gNAME_COL[gTEACHER_COL]},
                                    { data : gNAME_COL[gACTION_COL]},
                                ],
                                columnDefs : [
                                    {
                                        target : gACTION_COL,
                                        defaultContent : `
                                            <button class="btn btn-link btn-icon-edit btn-block data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="far fa-edit"></i></button>
                                        
                                            <button class="btn btn-link btn-icon-delete btn-block" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="far fa-trash-alt"></i></button>
                                        `
                                    },
                                
                                ]
                                });

    var gIDEditCourse = null

        // Region 2 

    onPageLoading()
    loadAllCoursesToTable(gDataTable)
    $('#btn-addcourse').on('click', function(){
        $('#modal-addcourse').modal('show')
    })
    $('#btn-submitcourse').on('click', function(){
        onBtnSubmitCourseClick()
    })
    $('#tbl-allcourses').on('click', '.btn-icon-edit', function(){
        $('#modal-editcourse').modal('show')
        onBtnIconEditClick(this)
    })
    $('#btn-submitcourse-edit').on('click', function(){
        onBtnSubmitCourseEditClick()
    })
        // Region 3

    function onPageLoading(){
        console.log('=== Page Loading ===')
        var vCouesesPopulerEle = $('.course-popular');
        var vCouesesTrendingEle = $('.course-trending');
        for( var bI = 0 ; bI < gCoursesDB.courses.length; bI++){
            // hiện courses có isPopular = true
            if( gCoursesDB.courses[bI].isPopular === true){
                var vNewDiv = $('<div>').addClass('col-md-3').appendTo(vCouesesPopulerEle)
                $('<img>')
                    .addClass('card card-img-top')
                    .attr('src', gCoursesDB.courses[bI].coverImage)
                    .appendTo(vNewDiv)
                var bDivContentBody = $('<div>').addClass('card-body')
                bDivContentBody.appendTo(vNewDiv)
                $('<a>')
                    .attr('href', '#')
                    .html('<b>' + gCoursesDB.courses[bI].courseName +'</b>')
                    .appendTo(bDivContentBody)
                $('<p>')
                    .html('<i class="far fa-clock"></i> ' + gCoursesDB.courses[bI].duration + ' ' + gCoursesDB.courses[bI].level)
                    .appendTo(bDivContentBody)
                $('<p>')
                    .html('<b>$' + gCoursesDB.courses[bI].discountPrice + '</b> &nbsp <span style="text-decoration: line-through">$' + gCoursesDB.courses[bI].price + '</span></p>')
                    .appendTo(bDivContentBody)

                var bDivContentFooter = $('<div>').addClass('card-footer text-center')
                bDivContentFooter.appendTo(vNewDiv)
                $('<p>')
                    .addClass('mt-3')
                    .html('<img src="' + gCoursesDB.courses[bI].teacherPhoto + '" style="width: 35px;" alt="" class="rounded-circle">&nbsp' + gCoursesDB.courses[bI].teacherName + '&nbsp <i class="fas fa-bookmark"></i>')
                    .appendTo(bDivContentFooter)
            }

            // hiện courses có isTrending = true
            if( gCoursesDB.courses[bI].isTrending === true){
                var vNewDiv = $('<div>').addClass('col-md-3').appendTo(vCouesesTrendingEle)
                $('<img>')
                    .addClass('card card-img-top')
                    .attr('src', gCoursesDB.courses[bI].coverImage)
                    .appendTo(vNewDiv)
                var bDivContentBody = $('<div>').addClass('card-body')
                bDivContentBody.appendTo(vNewDiv)
                $('<a>')
                    .attr('href', '#')
                    .html('<b>' + gCoursesDB.courses[bI].courseName +'</b>')
                    .appendTo(bDivContentBody)
                $('<p>')
                    .html('<i class="far fa-clock"></i> ' + gCoursesDB.courses[bI].duration + ' ' + gCoursesDB.courses[bI].level)
                    .appendTo(bDivContentBody)
                $('<p>')
                    .html('<b>$' + gCoursesDB.courses[bI].discountPrice + '</b> &nbsp <span style="text-decoration: line-through">$' + gCoursesDB.courses[bI].price + '</span></p>')
                    .appendTo(bDivContentBody)

                var bDivContentFooter = $('<div>').addClass('card-footer text-center')
                bDivContentFooter.appendTo(vNewDiv)
                $('<p>')
                    .addClass('mt-3')
                    .html('<img src="' + gCoursesDB.courses[bI].teacherPhoto + '" style="width: 35px;" alt="" class="rounded-circle">&nbsp' + gCoursesDB.courses[bI].teacherName + '&nbsp <i class="fas fa-bookmark"></i>')
                    .appendTo(bDivContentFooter)
            }
        }
    }

    function onBtnSubmitCourseClick(){
        console.log('AddCrouse Click')
        var vNewCourse = {
            id: -1,
            courseCode: "",
            courseName: "",
            price: -1,
            discountPrice: -1,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: false,
            isTrending: false
        }

        getDataNewCourseFormModal(vNewCourse)
        var vChecked = validateDataNewCourse(vNewCourse)
        if(vChecked){
            pushNewCourse(vNewCourse)
            processDisplayAddCourse()
        }
    }

    function onBtnSubmitCourseEditClick(){
        console.log('EditCrouse Click')
        var vEditCourse = {
            id: -1,
            courseCode: "",
            courseName: "",
            price: -1,
            discountPrice: -1,
            duration: "",
            level: "",
            coverImage: "",
            teacherName: "",
            teacherPhoto: "",
            isPopular: false,
            isTrending: false
        }
        getDataEditCourseFormModal(vEditCourse)
        var vChecked = validateDataEditCourse(vEditCourse)
        if(vChecked){
            var vDataCourseEdit = getCourseById(vEditCourse)
            vDataCourseEdit(vDataCourseEdit)
        }
    }
        // Region 4

    function onBtnIconEditClick(paramBtn){

        // get Data
        var vRowClick = $(paramBtn).closest('tr')
        var vTable = $('#tbl-allcourses').DataTable()
        var vDataRow = vTable.row(vRowClick).data()
        loadDataToFormEditCourse(vDataRow)
        gIDEditCourse = vDataRow.id
    }

    function loadDataToFormEditCourse(paramData){
        $('#inp-coursecode-edit').val(paramData.courseCode)
        $('#inp-coursename-edit').val(paramData.courseName)
        $('#inp-price-edit').val(paramData.price)
        $('#inp-discountprice-edit').val(paramData.discountPrice)
        $('#inp-duration-edit').val(paramData.duration)
        $('#inp-level-edit').val(paramData.level)
        $('#inp-teachername-edit').val(paramData.teacherName)
    }

    function processDisplayAddCourse(){
        console.log(JSON.stringify(gCoursesDB, true, 2))
        $('#modal-addcourse')
            .modal('hide')
        loadAllCoursesToTable(gDataTable)
        resertDataFormInputNewCourse()
    }

    function resertDataFormInputNewCourse(){
        $('#inp-coursecode').val('')
        $('#inp-coursename').val('')
        $('#inp-price').val('')
        $('#inp-discountprice').val('')
        $('#inp-duration').val('')
        $('#inp-level').val('')
        $('#inp-teachername').val('')
    }

    function pushNewCourse(paramNewCourse){
        gDataTable.push(paramNewCourse)
    }

    function vDataCourseEdit(paramEditCourse){
        
    }

    function getCourseById(paramEditCourse){
        var vIndex = 0;
        var vResult = false;
        var vData = null
        while(vIndex < gDataTable.length && vResult == false){
            if(paramEditCourse.id == gDataTable[vIndex].id){
                vResult = true
                vData = gDataTable[vIndex]
            }else{
                vIndex++
            }
        }
        return vData
    }

    function validateDataNewCourse(paramNewCourse){
        if(paramNewCourse.courseCode === ''){
            alert('Chưa nhập Course Code')
            return false
        }
        if(paramNewCourse.courseName === ''){
            alert('Chưa nhập Course Name')
            return false
        }
        if(paramNewCourse.price === ''){
            alert('Chưa nhập Price')
            return false
        }
        if(isNaN(paramNewCourse.price)){
            alert('Price chưa hợp lệ')
            return false
        }
        if(paramNewCourse.price < 0){
            alert('Price phải là số dương')
            return false
        }
        if(paramNewCourse.duration === ''){
            alert('Chưa nhập Duration')
            return false
        }
        if(paramNewCourse.level === ''){
            alert('Chưa nhập Level')
            return false
        }
        if(paramNewCourse.teacherName === ''){
            alert('Chưa nhập Teacher Name')
            return false
        }
        return true
    }

    function validateDataEditCourse(paramEditCourse){
        if(paramEditCourse.courseCode === ''){
            alert('Chưa nhập Course Code')
            return false
        }
        if(paramEditCourse.courseName === ''){
            alert('Chưa nhập Course Name')
            return false
        }
        if(paramEditCourse.price === ''){
            alert('Chưa nhập Price')
            return false
        }
        if(isNaN(paramEditCourse.price)){
            alert('Price chưa hợp lệ')
            return false
        }
        if(paramEditCourse.price < 0){
            alert('Price phải là số dương')
            return false
        }
        if(paramEditCourse.duration === ''){
            alert('Chưa nhập Duration')
            return false
        }
        if(paramEditCourse.level === ''){
            alert('Chưa nhập Level')
            return false
        }
        if(paramEditCourse.teacherName === ''){
            alert('Chưa nhập Teacher Name')
            return false
        }
        return true
    }

    function getDataNewCourseFormModal(paramNewCourse){
        paramNewCourse.id = getNextId()
        paramNewCourse.courseCode = $.trim($('#inp-coursecode').val())
        paramNewCourse.courseName = $.trim($('#inp-coursename').val())
        paramNewCourse.price = $.trim($('#inp-price').val())
        paramNewCourse.discountPrice = $.trim($('#inp-discountprice').val())
        paramNewCourse.duration = $.trim($('#inp-duration').val())
        paramNewCourse.level = $.trim($('#inp-level').val())
        paramNewCourse.teacherName = $.trim($('#inp-teachername').val())
    }

    function getDataEditCourseFormModal(paramEditCourse){
        paramEditCourse.id = gIDEditCourse
        paramEditCourse.courseCode = $.trim($('#inp-coursecode-edit').val())
        paramEditCourse.courseName = $.trim($('#inp-coursename-edit').val())
        paramEditCourse.price = $.trim($('#inp-price-edit').val())
        paramEditCourse.discountPrice = $.trim($('#inp-discountprice-edit').val())
        paramEditCourse.duration = $.trim($('#inp-duration-edit').val())
        paramEditCourse.level = $.trim($('#inp-level-edit').val())
        paramEditCourse.teacherName = $.trim($('#inp-teachername-edit').val())
    }

    function getNextId(){
        var vNextId = 0;
        if(gCoursesDB.courses.length == 0){
            vNextId = 1;
        }else{
            vNextId = gCoursesDB.courses[gCoursesDB.courses.length -1].id + 1;
        }
        return vNextId;
    }

    function loadAllCoursesToTable(paramCourse){
        gTableAllCourse.clear();
        gTableAllCourse.rows.add(paramCourse);
        gTableAllCourse.draw();
    }
})